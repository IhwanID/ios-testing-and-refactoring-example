//
//  ViewControllerTest.swift
//  JsonPlaceholderUserTests
//
//  Created by Ihwan on 08/10/21.
//

import XCTest
@testable import JsonPlaceholderUser

class ViewControllerTest: XCTestCase {

    func test_canInit() throws {
        _ = try makeSUT()
    }
    
    func test_viewDidLoad_setsTitle() throws {
        let sut = try makeSUT()
        
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.title, "Users")
    }
    
    func test_viewDidLoad_ConfigureTableView() throws {
        let sut = try makeSUT()
        
        sut.loadViewIfNeeded()
        
        XCTAssertNotNil(sut.tableView.delegate, "Delegate")
        XCTAssertNotNil(sut.tableView.delegate, "Data Source")
    }
    
    func test_viewDidLoad_initialState() throws {
        let sut = try makeSUT()
        
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.numberOfUsers(), 0)
    }
    
    func test_viewDidLoad_rendersUserFromApi() throws {
        let sut = try makeSUT()
        
        sut.getUsers = { completion in
            completion(.success([
            makeUser(id: 0, name: "Ihwan", email: "ihwan@apple.com"),
            makeUser(id: 1, name: "Andi", email: "andi@apple.com"),
            makeUser(id: 2, name: "Rangga", email: "rangga@apple.com")
            ]))
        }
        
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.numberOfUsers(), 3)
        XCTAssertEqual(sut.name(atRow: 0), "Ihwan")
        XCTAssertEqual(sut.email(atRow: 0), "ihwan@apple.com")
        
        XCTAssertEqual(sut.name(atRow: 1), "Andi")
        XCTAssertEqual(sut.email(atRow: 1), "andi@apple.com")
        
        XCTAssertEqual(sut.name(atRow: 2), "Rangga")
        XCTAssertEqual(sut.email(atRow: 2), "rangga@apple.com")
    }
    
    func test_viewDidLoad_whenUserNameStartWithC_highlightCell() throws {
        let sut = try makeSUT()
        
        sut.getUsers = { completion in
            completion(.success([
            makeUser(id: 0, name: "Candra Ihwan", email: "ihwan@apple.com"),
            makeUser(id: 1, name: "Andi Cahyadi", email: "andi@apple.com"),
            makeUser(id: 2, name: "Rangga", email: "rangga@apple.com")
            ]))
        }
        
        sut.loadViewIfNeeded()
        
        XCTAssertTrue(sut.isHighlighted(atRow: 0), "Cell 0")
        XCTAssertTrue(sut.isNotHighlighted(atRow: 1), "Cell 1")
        XCTAssertTrue(sut.isNotHighlighted(atRow: 2), "Cell 2")
        
    }
    
    func makeSUT() throws -> ViewController {
        let bundle = Bundle(for: ViewController.self)
        let sb = UIStoryboard(name: "Main", bundle: bundle)
        
        let initialVC = sb.instantiateInitialViewController()
        let navigation = try XCTUnwrap(initialVC as? UINavigationController)
        let sut = try XCTUnwrap(navigation.topViewController as? ViewController)
        return sut
    }
    
}

private func makeUser(id: Int, name: String, email: String) -> User {
    User(id: id, name: name, email: email)
}


private class APIManagerStub: ApiManager {
    override func getUsers(completion: @escaping (Result<[User], Error>) -> Void) {}
}


extension ViewController {
    func numberOfUsers() -> Int {
        tableView.numberOfRows(inSection: userSection)
    }
    
    func name(atRow row: Int) -> String? {
        userCell(atRow: row)?.nameLabel.text
    }
    
    func email(atRow row: Int) -> String? {
        userCell(atRow: row)?.emailLabel.text
    }
    
    func isHighlighted(atRow row: Int) -> Bool {
        userCell(atRow: row)?.backgroundColor == .green
    }
    
    func isNotHighlighted(atRow row: Int) -> Bool {
        userCell(atRow: row)?.backgroundColor == .white
    }
    
    func userCell(atRow row: Int) -> UserCell? {
        let ds = tableView.dataSource
        let indexPath = IndexPath(row: row, section: userSection)
        return ds?.tableView(tableView, cellForRowAt: indexPath) as? UserCell
    }
    
    private var userSection: Int { 0 }
    
}
