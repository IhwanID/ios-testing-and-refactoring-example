//
//  APIManager.swift
//  JsonPlaceholderUser
//
//  Created by Ihwan on 08/10/21.
//

import Foundation

struct User: Decodable {
    let id: Int
    let name: String
    let email: String
}

class ApiManager {
    
    static let shared = ApiManager()
    
    private init() {}
    
    func getUsers(completion: @escaping (Result<[User], Error>) -> Void ){
        //1.url
        let urlString = "https://jsonplaceholder.typicode.com/users"
        
        let url = URL(string: urlString)!
        
        //2. session
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            //3.handle error
            if let error = error{
                completion(.failure(error))
            }
            //4.handle data
            guard let data = data else{
                return
            }
            //5. handle decoding
            do{
                let users = try JSONDecoder().decode([User].self, from: data)
                completion(.success(users))
            }
            catch (let error){
                completion(.failure(error))
            }
        }.resume()
    }
}
