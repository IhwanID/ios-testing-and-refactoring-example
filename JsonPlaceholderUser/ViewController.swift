//
//  ViewController.swift
//  JsonPlaceholderUser
//
//  Created by Ihwan on 08/10/21.
//

import UIKit

typealias GetUsers = (@escaping (Result<[User], Error>) -> Void) -> Void
class ViewController: UIViewController {

    var getUsers: GetUsers = { completion in
        ApiManager.shared.getUsers { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    private var users: [User] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Users"
        
        configureTableView()
               
        getUsersFromSingleTone()
        
    }

    
    func getUsersFromSingleTone(){
            getUsers { (result) in
                switch result{
                case .failure(let error):
                    print(error.localizedDescription)
                case.success(let users):
                        self.users = users
                        self.tableView.reloadData()
                }
            }
        }
        
        func configureTableView(){
            tableView.delegate = self
            tableView.dataSource = self
        }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? UserCell
        
        cell?.nameLabel.text = users[indexPath.row].name
        cell?.emailLabel.text = users[indexPath.row].email
        
        if users[indexPath.row].name.starts(with: "C"){
            cell?.backgroundColor = .green
        }
        else{
            cell?.backgroundColor = .white
        }
        
        return cell ?? UITableViewCell()
    }
    
    
}
